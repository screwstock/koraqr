package com.blivic.koraqr;

import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.transition.TransitionManager;


public class StartScreen extends ActionBarActivity {

    ViewGroup RelLay;
    Button button;
    TextView kora;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
        button = (Button) findViewById(R.id.button);
        kora = (TextView) findViewById(R.id.kora);
        RelLay = (ViewGroup) findViewById(R.id.RelLay);

        button.setVisibility(View.INVISIBLE);

        RelLay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                //gesture detector to detect swipe.
                showButton();
                return true;//always return true to consume event
            }
        });


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        //Set Roboto Light to both kora and button.
        Typeface robotoF= Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        kora.setTypeface(robotoF);
        button.setTypeface(robotoF);

    }

    public void showButton(){

        //TransitionManager.beginDelayedTransition(RelLay);
        //Un-comment the above line to see button. Requires API Level 19+

        //Moving 'kora' title to top and resize.

        button.setVisibility(View.VISIBLE);
        kora.setTextSize(30);



        RelativeLayout.LayoutParams setPositionForK = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        setPositionForK.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        setPositionForK.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        kora.setLayoutParams(setPositionForK);


        //Moving button to center by making it visible.
        RelativeLayout.LayoutParams setPosition = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        setPosition.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        button.setLayoutParams(setPosition);





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
